package u02lab.code;

/**
 * Created by OPeratore on 08/03/2017.
 */
public interface SequenceGeneratorFactory {
    SequenceGenerator createRandomGenerator(int numberOfBits);
    SequenceGenerator createRangeGenerator(int start,int stop);
}
