package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 * <p>
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator{
    private int counter;
    private int start;
    private int stop;
    private List<Integer> listOfNumbers;

    public RangeGenerator(int start, int stop) {
        this.stop = stop;
        this.start = start;
        this.counter = start;
        fillList(start);


    }

    @Override
    public Optional<Integer> next() {


        fillList(counter + 1);
        if (this.isOver())
            return Optional.empty();


        return Optional.of(counter++);
    }

    @Override
    public void reset() {
        counter = start;

    }

    @Override
    public boolean isOver() {
        if (counter > stop)
            return true;
        return false;
    }

    @Override
    public List<Integer> allRemaining() {

        return listOfNumbers;
    }


    private void fillList(int index) {
        listOfNumbers = new ArrayList<>();
        if(!this.isOver()) {
            for (int i = index; i <= stop; i++) {
                listOfNumbers.add(i);
            }
        }


    }
}
