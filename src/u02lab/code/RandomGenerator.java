package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 * <p>
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 * <p>
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {
    private List<Integer> listOfBits;
    private int numberOfRandomBits;
    private int counter;

    public RandomGenerator(int n) {
        this.numberOfRandomBits = n;
        this.counter = 0;
        fillList(counter);

    }


    @Override
    public Optional<Integer> next() {
        counter++;
        fillList(counter);
        if (!this.isOver()) {
            return Optional.of(randomBit());
        } else return Optional.empty();
    }

    @Override
    public void reset() {
        counter = 0;
        fillList(counter);
    }

    @Override
    public boolean isOver() {
        if (counter < numberOfRandomBits)
            return false;
        else return true;
    }

    @Override
    public List<Integer> allRemaining() {
        return listOfBits;
    }

    private Integer randomBit() {
        Double random = Math.random();
        Long bit = Math.round(random);
        return bit.intValue();
    }

    private void fillList(int index) {
        listOfBits = new ArrayList<>();
        if (index < numberOfRandomBits) {
            for (int i = index; i < numberOfRandomBits; i++) {

                listOfBits.add(randomBit());
            }
        }


    }
}
