package u02lab.code;

/**
 * Created by OPeratore on 08/03/2017.
 */
public class SequenceGeneratorFactoryImpl implements SequenceGeneratorFactory{
    @Override
    public SequenceGenerator createRandomGenerator(int numberOfBits) {
        return new RandomGenerator(numberOfBits);
    }

    @Override
    public SequenceGenerator createRangeGenerator(int start,int stop) {
        return new RangeGenerator(start,stop);
    }
}
