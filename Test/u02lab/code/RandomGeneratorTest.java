package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by OPeratore on 07/03/2017.
 */
public class RandomGeneratorTest {
    private final static int BITS = 6;
    private SequenceGeneratorFactory factory=new SequenceGeneratorFactoryImpl();
    private SequenceGenerator sequenceGenerator;

    @Before
    public void setUp() throws Exception {
        sequenceGenerator = factory.createRandomGenerator(BITS);
    }

    @Test
    public void checkNumbersAreBits() {
       Optional<Integer> bit=sequenceGenerator.next();
            assertTrue(bit.equals(Optional.of(1)) || bit.equals(Optional.of(0)));


    }

    @Test
    public void next() throws Exception {
        for (int i = 1; i <BITS; i++)
            assertTrue(sequenceGenerator.next().isPresent());
        assertTrue(sequenceGenerator.next().equals(Optional.empty()));

    }

    @Test
    public void reset() throws Exception {
        sequenceGenerator.next();
        sequenceGenerator.reset();
        assertTrue(sequenceGenerator.allRemaining().size() == BITS);
    }

    @Test
    public void isOverReturnsTrue() throws Exception {
        for (int i = 1; i <= BITS; i++)
            sequenceGenerator.next();
        assertTrue(sequenceGenerator.isOver() == true);
    }

    @Test
    public void isOverReturnsFalse() throws Exception {
        sequenceGenerator.next();
        assertTrue(sequenceGenerator.isOver() == false);
    }

    @Test
    public void allRemainingSizeIsZero() throws Exception {
        this.next();
        assertTrue(sequenceGenerator.allRemaining().size()==0);
    }
    @Test
    public void allRemaining() throws Exception{
        sequenceGenerator.next();
        assertTrue(sequenceGenerator.allRemaining().size()==(BITS-1));
    }

}