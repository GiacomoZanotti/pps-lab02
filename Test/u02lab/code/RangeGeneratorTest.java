package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by OPeratore on 07/03/2017.
 */
public class RangeGeneratorTest {
    private static final int START = 3;
    private static final int STOP = 7;
    private SequenceGenerator sequenceGenerator;
    private SequenceGeneratorFactory factory = new SequenceGeneratorFactoryImpl();
    private int testCounter;

    @Before
    public void setUp() throws Exception {
        sequenceGenerator = factory.createRangeGenerator(START, STOP);
        testCounter=START;

    }

    @Test
    public void instanceOfRangeGenerator() throws Exception {
        assertTrue(sequenceGenerator instanceof RangeGenerator);


    }

    @Test
    public void next() throws Exception {
        for (int i = START; i <= STOP; i++) {
            assertTrue(sequenceGenerator.next().equals(Optional.of(i)));
        }
        assertTrue(sequenceGenerator.next().equals(Optional.empty()));

    }

    @Test
    public void reset() throws Exception {
        sequenceGenerator.next();
        sequenceGenerator.reset();
        assertTrue(sequenceGenerator.next().equals(Optional.of(START)));

    }

    @Test
    public void isOverReturnsTrue() throws Exception {
        this.next();
        assertTrue(sequenceGenerator.isOver() == true);

    }

    @Test
    public void isOverReturnsFalse() throws Exception {
        sequenceGenerator.next();
        assertTrue(sequenceGenerator.isOver() == false);
    }

    @Test
    public void allRemaining() throws Exception {
        sequenceGenerator.next();
        testCounter++;
        this.allRemainingBeforeNext();


    }

    @Test
    public void allRemainingBeforeNext() throws Exception {
        List<Integer> listOfRemainingNumbers = sequenceGenerator.allRemaining();
        int index = 0;
        for (int i = testCounter; i <= STOP; i++) {
            assertTrue(listOfRemainingNumbers.get(index++).equals(i));
        }
    }
    @Test
    public void allRemainingSizeZero() throws Exception{
        this.next();
        assertTrue(sequenceGenerator.allRemaining().size()==0);
    }

}